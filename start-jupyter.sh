#!/bin/bash

echo "Starting Jupyter..."

jupyter notebook "$@" --allow-root
