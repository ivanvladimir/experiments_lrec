FROM tensorflow/tensorflow:1.12.0-gpu-py3

RUN apt-get update && \
apt-get install git -y

RUN mkdir -p /home/tensor
RUN mkdir -p /home/tensor/notebooks
RUN mkdir -p /recordings/voxforge
RUN mkdir -p /recordings/librivox
RUN mkdir -p /recordings/speakers

WORKDIR /home/tensor
ADD requirements.txt /home/tensor

# Installing Essential
run apt-get install build-essential libyaml-dev libfftw3-dev libavcodec-dev libavformat-dev libavutil-dev libavresample-dev python-dev libsamplerate0-dev libtag1-dev libchromaprint-dev -y && \
apt-get install python-dev python-numpy-dev python-numpy python-yaml -y && \
apt-get install git -y

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY notebooks/* /home/tensor/notebooks/

#CLEANUP
RUN rm -rf /root/.cache/pip/* && \
apt-get autoremove -y && \
apt-get clean && \
rm -rf /usr/local/src/*

RUN apt-get install supervisor -y

COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

RUN mkdir -p /root/.jupyter
RUN echo c.NotebookApp.ip = \'0.0.0.0\' > /root/.jupyter/jupyter_notebook_config.py
ADD /start-jupyter.sh /

CMD ["/usr/bin/supervisord"]
